function cacheFunction(cb){
    try{
        let cache={};
        function cacheIn(...args){
            let key=JSON.stringify(args);
            if(key in cache){
                return `already exists:${cache[key]}`;
            }else{
                cache[key]=cb(...args);
                return `adding value to key:${cb(...args)}`;

            }
        }
        return cacheIn;

    }catch(error){
        console.log(`error occured : ${error}`);
    }
};


module.exports=cacheFunction;
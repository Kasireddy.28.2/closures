let cache=require("../cacheFuction.cjs");

function sum(a,b){
    return a+b;
};

let cachedFunction=cache(sum);

console.log(cachedFunction(1,2));
console.log(cachedFunction(2,5));
console.log(cachedFunction(1,2));
function counterFactory(){
    try{
        let count=0;
        function increment(){
            count++
            return count;
        };
        function decrement(){
            count--
            return count;
        };
        return {
            increment,
            decrement
        }

    }catch(error){
        console.log(`error occured : ${error}`);
    }

    
};

module.exports=counterFactory;
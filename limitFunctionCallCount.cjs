function limitFunctionCallCount(cb,n){
    try{
        let count=0;
    function limitingCallBackFunctioning(){
        if(count<n){
            count++
            cb();
        }else{
            console.log(`n times reached`);
        }

    }
    return limitingCallBackFunctioning;

    }catch(error){
        console.log(`error occured : ${error}`);
    }

};

module.exports=limitFunctionCallCount;